package ci.smile.formationEcole.rest.api;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.CannotCreateTransactionException;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ci.smile.formationEcole.business.ClasseMatiereBusiness;
import ci.smile.formationEcole.utils.ExceptionUtils;
import ci.smile.formationEcole.utils.FunctionalError;
import ci.smile.formationEcole.utils.StatusCode;
import ci.smile.formationEcole.utils.StatusMessage;
import ci.smile.formationEcole.utils.Validate;
import ci.smile.formationEcole.utils.contract.Request;
import ci.smile.formationEcole.utils.contract.Response;
import ci.smile.formationEcole.utils.dto.ClasseMatiereDto;

@CrossOrigin("*")
@RestController
@RequestMapping(value = "/classematiere")
public class ClasseMatiereController {

	@Autowired
	private ClasseMatiereBusiness classematiereBusiness;

	@Autowired
	private FunctionalError functionalError;

	@Autowired
	private ExceptionUtils exceptionUtils;

	private Logger slf4jLogger = LoggerFactory.getLogger(getClass());

	@Autowired
	private HttpServletRequest requestBasic;

	@RequestMapping(value = "/create", method = RequestMethod.POST, consumes = { "application/json" }, produces = {
			"application/json" })
	public Response<ClasseMatiereDto> create(@RequestBody Request<ClasseMatiereDto> request) {
		slf4jLogger.info("start method /classematiere/create");
		Response<ClasseMatiereDto> response = new Response<ClasseMatiereDto>();

		String languageID = (String) requestBasic.getAttribute("CURRENT_LANGUAGE_IDENTIFIER");
		Locale locale = new Locale(languageID, "");

		try {

			response = Validate.validateList(request, response, functionalError, locale);
			if (!response.isHasError()) {
				response = classematiereBusiness.create(request, locale);
			} else {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(),
						response.getStatus().getMessage());
				return response;
			}

			if (!response.isHasError()) {
				response.setStatus(functionalError.SUCCESS("", locale));
				slf4jLogger.info("end method create");
				slf4jLogger.info("code: {} -  message: {}", StatusCode.SUCCESS, StatusMessage.SUCCESS);
			} else {
				slf4jLogger.info("Erreur| code: {} -  message: {}", response.getStatus().getCode(),
						response.getStatus().getMessage());
			}

		} catch (CannotCreateTransactionException e) {
			exceptionUtils.CANNOT_CREATE_TRANSACTION_EXCEPTION(response, locale, e);
		} catch (TransactionSystemException e) {
			exceptionUtils.TRANSACTION_SYSTEM_EXCEPTION(response, locale, e);
//  }catch (StripeException e) {
//   exceptionUtils.STRIPE_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		}
		slf4jLogger.info("end method /classematiere/create");
		return response;
	}
}
