package ci.smile.formationEcole.business;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import ci.smile.formationEcole.dao.entity.Classe;
import ci.smile.formationEcole.dao.entity.ClasseMatiere;
import ci.smile.formationEcole.dao.entity.Matiere;
import ci.smile.formationEcole.dao.repository.IClasseMatiereRepository;
import ci.smile.formationEcole.dao.repository.IClasseRepository;
import ci.smile.formationEcole.dao.repository.IMatiereRepository;
import ci.smile.formationEcole.utils.ExceptionUtils;
import ci.smile.formationEcole.utils.FunctionalError;
import ci.smile.formationEcole.utils.Validate;
import ci.smile.formationEcole.utils.contract.IBasicBusiness;
import ci.smile.formationEcole.utils.contract.Request;
import ci.smile.formationEcole.utils.contract.Response;
import ci.smile.formationEcole.utils.dto.ClasseMatiereDto;
import ci.smile.formationEcole.utils.dto.transformer.IClasseMatiereTransformer;

@Component
public class ClasseMatiereBusiness implements IBasicBusiness<Request<ClasseMatiereDto>, Response<ClasseMatiereDto>> {

	private Response<ClasseMatiereDto> response;
	
	@Autowired
	private FunctionalError functionalError;
	
	@Autowired
	private IMatiereRepository iMatiereRepository;
	
	@Autowired
	private IClasseRepository iClasseRepository;
	
	@Autowired
	private IClasseMatiereRepository iClasseMatiereRepository;
	
	@Autowired
	private ExceptionUtils exceptionUtils;

	@Override
	@Transactional(rollbackFor = {RuntimeException.class, Exception.class})
	public Response<ClasseMatiereDto> create(Request<ClasseMatiereDto> request, Locale locale) {
		// TODO Auto-generated method stub
		//
		response = new Response<>();
		List<ClasseMatiere> items = new ArrayList<ClasseMatiere>();
		try {
			for (ClasseMatiereDto dto : request.getDatas()) {
	
				// champs coefficient et nombre de place obligatoire
				Map<String, Object> mandatoryField = new HashMap<String, Object>();
				mandatoryField.put("coefficient", dto.getCoefficient());
				if (!Validate.RequiredValue(mandatoryField).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}
	
				// ne pas tenir compte de l'id si renseigné
				dto.setId(null);
				
				Matiere existingMatiere = null;
				if (dto.getMatiereId() != null && dto.getMatiereId() > 0) {
					existingMatiere = iMatiereRepository.findOne(dto.getMatiereId());
					if (existingMatiere == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("matiereId -> " + dto.getMatiereId(), locale));
						response.setHasError(true);
						return response;
					}
				}
				// Vérifier si class_id est l'id d'une classe
				Classe existingClasse = null;
				if (dto.getClasseId() != null && dto.getClasseId() > 0) {
					existingClasse = iClasseRepository.findOne(dto.getClasseId());
					if (existingClasse == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("classeId -> " + dto.getClasseId(), locale));
						response.setHasError(true);
						return response;
					}
				}
				
				// transformation de l'objet Dto en entity
				ClasseMatiere entityToSave = IClasseMatiereTransformer.INSTANCE.toEntity(dto);
				
				items.add(entityToSave);
	
			}
			if (!items.isEmpty()) {
				// enregistrer la liste d'objets ClasseMatiereDto transformé en ClasseMatiere
				List<ClasseMatiere> entitiesSaved = iClasseMatiereRepository.saveAll(items);

				response.setStatus(functionalError.SUCCESS("create classe matiere", locale));
				response.setHasError(false);

				// retoune au front-end les infos sur les eleves enregistrés
				response.setItems(IClasseMatiereTransformer.INSTANCE.toDtos(entitiesSaved));
			}

		 } catch (PermissionDeniedDataAccessException e) {
	            exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
	        } catch (DataAccessResourceFailureException e) {
	            exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
	        } catch (DataAccessException e) {
	            exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
	        } catch (RuntimeException e) {
	            exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
	        } catch (Exception e) {
	            exceptionUtils.EXCEPTION(response, locale, e);
	        } finally {
	            if (response.isHasError() && response.getStatus() != null) {
	                System.out.println(String.format("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage()));
	                throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
	            }
	        }
			
		
		return response;

	}

	@Override
	public Response<ClasseMatiereDto> update(Request<ClasseMatiereDto> request, Locale locale) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Response<ClasseMatiereDto> delete(Request<ClasseMatiereDto> request, Locale locale) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Response<ClasseMatiereDto> getByCriteria(Request<ClasseMatiereDto> request, Locale locale) {
		// TODO Auto-generated method stub
		return null;
	}

}
