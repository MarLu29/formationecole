package ci.smile.formationEcole.business;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import ci.smile.formationEcole.dao.entity.Classe;
import ci.smile.formationEcole.dao.entity.Eleve;
import ci.smile.formationEcole.dao.repository.IClasseRepository;
import ci.smile.formationEcole.dao.repository.IEleveRepository;
import ci.smile.formationEcole.utils.ExceptionUtils;
import ci.smile.formationEcole.utils.FunctionalError;
import ci.smile.formationEcole.utils.Utilities;
import ci.smile.formationEcole.utils.Validate;
import ci.smile.formationEcole.utils.contract.IBasicBusiness;
import ci.smile.formationEcole.utils.contract.Request;
import ci.smile.formationEcole.utils.contract.Response;
import ci.smile.formationEcole.utils.dto.EleveDto;
import ci.smile.formationEcole.utils.dto.transformer.IEleveTransformer;
import ci.smile.formationEcole.utils.enums.SexeEnum;

@Component
public class EleveBusiness implements IBasicBusiness<Request<EleveDto>, Response<EleveDto>> {

	private Response<EleveDto> response;

	@Autowired
	private FunctionalError functionalError;

	@Autowired
	private ExceptionUtils exceptionUtils;

	@Autowired
	private IEleveRepository iEleveRepository;

	@Autowired
	private IClasseRepository iClasseRepository;

	@Override
    @Transactional(rollbackFor = {RuntimeException.class, Exception.class})
	public Response<EleveDto> create(Request<EleveDto> request, Locale locale) {

		response = new Response<>();
		List<Eleve> items = new ArrayList<Eleve>();
		// TODO Auto-generated method stub
		//
		try {
			for (EleveDto dto : request.getDatas()) {

				// champs obligatoires: matricule, nom, prenom
				Map<String, Object> mandatoryField = new HashMap<String, Object>();
				mandatoryField.put("matricule", dto.getMatricule());
				mandatoryField.put("nom", dto.getNom());
				mandatoryField.put("prenom", dto.getPrenom());
				if (!Validate.RequiredValue(mandatoryField).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				// ne pas tenir de l'id si renseigné
				dto.setId(null);
				if (Utilities.notBlank(dto.getSexe())) {
					boolean ok = false;
					// sexe IN [Masculin,M,Femimin,F]
					for (SexeEnum sex : SexeEnum.values()) {
						if (dto.getSexe().equals(sex.name()))
							ok = true;

					}
					if (!ok) {
						response.setStatus(functionalError.REQUEST_FAIL("valeurs requises pour sexe [Masculin,M,Feminin,F]", locale));
						response.setHasError(true);
						return response;
					}
				}
				// format dateNaissance (dd/MM/yyyy)

				// format particulier pour le matricule (2020-(XXXXX)

				// unicité du matricule, sinon message d'erreur
				Eleve existingEntity = iEleveRepository.findByMatricule(dto.getMatricule());
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("matricule -> " + dto.getMatricule(), locale));
					response.setHasError(true);
					return response;
				}

				// classid entier,doit correspondre à l'identifiant d'une classe
				Classe existingClasse = null;
				if (dto.getClasseId() != null && dto.getClasseId() > 0) {
					existingClasse = iClasseRepository.findOne(dto.getClasseId());
					if (existingClasse == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("classeId -> " + dto.getClasseId(), locale));
						response.setHasError(true);
						return response;
					}
				}

				// transformation de l'objet Dto en entity
				Eleve entityToSave = IEleveTransformer.INSTANCE.toEntity(dto, existingClasse);

				items.add(entityToSave);

			}
			if (!items.isEmpty()) {
				// enregistrer la liste d'objets EleveDto transformé en Eleve
				List<Eleve> entitiesSaved = iEleveRepository.saveAll(items);

				response.setStatus(functionalError.SUCCESS("create eleve", locale));
				response.setHasError(false);

				// retoune au front-end les infos sur les eleves enregistrés
				response.setItems(IEleveTransformer.INSTANCE.toDtos(entitiesSaved));
			}

		 } catch (PermissionDeniedDataAccessException e) {
	            exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
	        } catch (DataAccessResourceFailureException e) {
	            exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
	        } catch (DataAccessException e) {
	            exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
	        } catch (RuntimeException e) {
	            exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
	        } catch (Exception e) {
	            exceptionUtils.EXCEPTION(response, locale, e);
	        } finally {
	            if (response.isHasError() && response.getStatus() != null) {
	                System.out.println(String.format("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage()));
	                throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
	            }
	        }
		return response;
	}

	@Override
	public Response<EleveDto> update(Request<EleveDto> request, Locale locale) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Response<EleveDto> delete(Request<EleveDto> request, Locale locale) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Response<EleveDto> getByCriteria(Request<EleveDto> request, Locale locale) {
		// TODO Auto-generated method stub
		return null;
	}

}
