package ci.smile.formationEcole.business;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


import org.springframework.transaction.annotation.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;

import ci.smile.formationEcole.dao.entity.Classe;
import ci.smile.formationEcole.dao.repository.IClasseRepository;
import ci.smile.formationEcole.utils.ExceptionUtils;
import ci.smile.formationEcole.utils.FunctionalError;
import ci.smile.formationEcole.utils.Validate;
import ci.smile.formationEcole.utils.contract.IBasicBusiness;
import ci.smile.formationEcole.utils.contract.Request;
import ci.smile.formationEcole.utils.contract.Response;
import ci.smile.formationEcole.utils.dto.ClasseDto;
import ci.smile.formationEcole.utils.dto.transformer.IClasseTransformer;

@Component
@Transactional(rollbackFor = {RuntimeException.class, Exception.class})

public class ClasseBusiness implements IBasicBusiness<Request<ClasseDto>, Response<ClasseDto>> {

	private Response<ClasseDto> response;

	@Autowired
	private FunctionalError functionalError;
	
	@Autowired
	private ExceptionUtils exceptionUtils;

	@Autowired
	private IClasseRepository iClasseRepository;

	@Override
	public Response<ClasseDto> create(Request<ClasseDto> request, Locale locale) {
		// TODO Auto-generated method stub
		//
		response = new Response<>();
		List<Classe> items = new ArrayList<Classe>();
		try {
			for (ClasseDto dto : request.getDatas()) {
	
				// champs libelle et nombre de place obligatoire
				Map<String, Object> mandatoryField = new HashMap<String, Object>();
				mandatoryField.put("libelle", dto.getLibelle());
				mandatoryField.put("nombrePlace", dto.getNombrePlace());
				if (!Validate.RequiredValue(mandatoryField).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}
	
				// ne pas tenir compte de l'id si renseigné
				dto.setId(null);
				
	
				Classe entityToSave = IClasseTransformer.INSTANCE.toEntity(dto);
				
				items.add(entityToSave);
			}
			if (!items.isEmpty()) {
				// enregistrer la liste d'objets ClasseDto transformé en Classe
				List<Classe> entitiesSaved = iClasseRepository.saveAll(items);
	
				response.setStatus(functionalError.SUCCESS("create classe", locale));
				response.setHasError(false);
	
				// retoune au front-end les infos sur les eleves enregistrés
				response.setItems(IClasseTransformer.INSTANCE.toDtos(entitiesSaved));
			}
			
		}catch (PermissionDeniedDataAccessException e) {
            exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
        } catch (DataAccessResourceFailureException e) {
            exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
        } catch (DataAccessException e) {
            exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
        } catch (RuntimeException e) {
            exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
        } catch (Exception e) {
            exceptionUtils.EXCEPTION(response, locale, e);
        } finally {
            if (response.isHasError() && response.getStatus() != null) {
                System.out.println(String.format("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage()));
                throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
            }
        }
		
		return response;

	}

	@Override
	public Response<ClasseDto> update(Request<ClasseDto> request, Locale locale) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Response<ClasseDto> delete(Request<ClasseDto> request, Locale locale) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Response<ClasseDto> getByCriteria(Request<ClasseDto> request, Locale locale) {
		// TODO Auto-generated method stub
		return null;
	}

}
