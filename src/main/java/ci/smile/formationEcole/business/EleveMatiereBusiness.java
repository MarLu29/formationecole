package ci.smile.formationEcole.business;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
//Ajouter cet import les transactions
import org.springframework.transaction.annotation.Transactional;

import ci.smile.formationEcole.dao.entity.Eleve;
import ci.smile.formationEcole.dao.entity.EleveMatiere;
import ci.smile.formationEcole.dao.entity.Matiere;
import ci.smile.formationEcole.dao.repository.IEleveMatiereRepository;
import ci.smile.formationEcole.dao.repository.IEleveRepository;
import ci.smile.formationEcole.dao.repository.IMatiereRepository;
import ci.smile.formationEcole.utils.ExceptionUtils;
import ci.smile.formationEcole.utils.FunctionalError;
import ci.smile.formationEcole.utils.Validate;
import ci.smile.formationEcole.utils.contract.IBasicBusiness;
import ci.smile.formationEcole.utils.contract.Request;
import ci.smile.formationEcole.utils.contract.Response;
import ci.smile.formationEcole.utils.dto.EleveMatiereDto;
import ci.smile.formationEcole.utils.dto.transformer.IEleveMatiereTransformer;
//Je viens d'apprendre git
//Je fais une modif git 
@Component
@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
public class EleveMatiereBusiness implements IBasicBusiness<Request<EleveMatiereDto>, Response<EleveMatiereDto>> {

	private Response<EleveMatiereDto> response;
	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private IMatiereRepository iMatiereRepository;
	@Autowired
	private IEleveRepository iEleveRepository;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@Autowired
	private IEleveMatiereRepository iEleveMatiereRepository;

	@Override
	public Response<EleveMatiereDto> create(Request<EleveMatiereDto> request, Locale locale) {
		// TODO Auto-generated method stub
		//
		response = new Response<>();
		List<EleveMatiere> items = new ArrayList<EleveMatiere>();
		try {
			for (EleveMatiereDto dto : request.getDatas()) {
	
				// champs obligatoires: matricule, nom, prenom
				Map<String, Object> mandatoryField = new HashMap<String, Object>();
				mandatoryField.put("note", dto.getNote());
				if (!Validate.RequiredValue(mandatoryField).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}
	
				// ne pas tenir compte de l'id si renseigné
				dto.setId(null);
				
				Eleve existingEleve = null;
				if (dto.getEleveId() != null && dto.getEleveId() > 0) {
					existingEleve = iEleveRepository.findOne(dto.getEleveId());
					if (existingEleve == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("eleveId -> " + dto.getEleveId(), locale));
						response.setHasError(true);
						return response;
					}
				}
				
				Matiere existingMatiere = null;
				if (dto.getMatiereId() != null && dto.getMatiereId() > 0) {
					existingMatiere = iMatiereRepository.findOne(dto.getMatiereId());
					if (existingMatiere == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("matiereId  -> " + dto.getMatiereId(), locale));
						response.setHasError(true);
						return response;
					}
				}
				// transformation de l'objet Dto en entity
				EleveMatiere entityToSave = IEleveMatiereTransformer.INSTANCE.toEntity(dto);
				
				items.add(entityToSave);
			}
			if (!items.isEmpty()) {
				List<EleveMatiere> entitiesSaved = iEleveMatiereRepository.saveAll(items);
				
				response.setStatus(functionalError.SUCCESS("create eleve matiere", locale));
				response.setHasError(false);
				
				response.setItems(IEleveMatiereTransformer.INSTANCE.toDtos(entitiesSaved));
			}
		} catch (PermissionDeniedDataAccessException e) {
            exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
        } catch (DataAccessResourceFailureException e) {
            exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
        } catch (DataAccessException e) {
            exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
        } catch (RuntimeException e) {
            exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
        } catch (Exception e) {
            exceptionUtils.EXCEPTION(response, locale, e);
        } finally {
            if (response.isHasError() && response.getStatus() != null) {
                System.out.println(String.format("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage()));
                throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
            }
        }
		return response;

	}

	@Override
	public Response<EleveMatiereDto> update(Request<EleveMatiereDto> request, Locale locale) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Response<EleveMatiereDto> delete(Request<EleveMatiereDto> request, Locale locale) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Response<EleveMatiereDto> getByCriteria(Request<EleveMatiereDto> request, Locale locale) {
		// TODO Auto-generated method stub
		return null;
	}

}
