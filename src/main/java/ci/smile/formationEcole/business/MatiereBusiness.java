package ci.smile.formationEcole.business;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import ci.smile.formationEcole.dao.entity.Matiere;
import ci.smile.formationEcole.dao.repository.IMatiereRepository;
import ci.smile.formationEcole.utils.ExceptionUtils;
import ci.smile.formationEcole.utils.FunctionalError;
import ci.smile.formationEcole.utils.Validate;
import ci.smile.formationEcole.utils.contract.IBasicBusiness;
import ci.smile.formationEcole.utils.contract.Request;
import ci.smile.formationEcole.utils.contract.Response;
import ci.smile.formationEcole.utils.dto.MatiereDto;
import ci.smile.formationEcole.utils.dto.transformer.IMatiereTransformer;
@Component
@Transactional(rollbackFor = {RuntimeException.class, Exception.class})
public class MatiereBusiness implements IBasicBusiness<Request<MatiereDto>, Response<MatiereDto>> {

	private Response<MatiereDto> response;
	
	@Autowired
	private FunctionalError functionalError;
	
	@Autowired
	private IMatiereRepository iMatiereRepository;
	
	@Autowired
	private ExceptionUtils exceptionUtils;
	
	@Override
	public Response<MatiereDto> create(Request<MatiereDto> request, Locale locale) {
		// TODO Auto-generated method stub
		//
		response = new Response<>();
		List<Matiere> items = new ArrayList<Matiere>();
		try {
		for (MatiereDto dto : request.getDatas()) {

			// champs libelle et nombre de place obligatoire
			Map<String, Object> mandatoryField = new HashMap<String, Object>();
			mandatoryField.put("libelle", dto.getLibelle());
			if (!Validate.RequiredValue(mandatoryField).isGood()) {
				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
				response.setHasError(true);
				return response;
			}

			// ne pas tenir compte de l'id si renseigné
			dto.setId(null);
			
			Matiere entityToSave = IMatiereTransformer.INSTANCE.toEntity(dto);
			
			items.add(entityToSave);
			

		}
		//A ajouter pour la consommation du web service
		if (!items.isEmpty()) {
			// enregistrer la liste d'objets MatiereDto transformé en Matiere
			List<Matiere> entitiesSaved = iMatiereRepository.saveAll(items);

			response.setStatus(functionalError.SUCCESS("create matiere", locale));
			response.setHasError(false);

			// retoune au front-end les infos sur les eleves enregistrés
			response.setItems(IMatiereTransformer.INSTANCE.toDtos(entitiesSaved));
		}
		}catch (PermissionDeniedDataAccessException e) {
            exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
        } catch (DataAccessResourceFailureException e) {
            exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
        } catch (DataAccessException e) {
            exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
        } catch (RuntimeException e) {
            exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
        } catch (Exception e) {
            exceptionUtils.EXCEPTION(response, locale, e);
        } finally {
            if (response.isHasError() && response.getStatus() != null) {
                System.out.println(String.format("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage()));
                throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
            }
        }
		return response;

	}

	@Override
	public Response<MatiereDto> update(Request<MatiereDto> request, Locale locale) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Response<MatiereDto> delete(Request<MatiereDto> request, Locale locale) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Response<MatiereDto> getByCriteria(Request<MatiereDto> request, Locale locale) {
		// TODO Auto-generated method stub
		return null;
	}

}
