package ci.smile.formationEcole.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.smile.formationEcole.dao.entity.Eleve;

@Repository
public interface IEleveRepository extends JpaRepository<Eleve, Integer> {

	@Query("Select e From Eleve e Where e.matricule = :matricule ")
	Eleve findByMatricule(@Param("matricule")String matricule);
	
	@Query("select e from Eleve e where e.id = :id")
	Eleve findOne(@Param("id")Integer id);
}
