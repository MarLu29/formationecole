package ci.smile.formationEcole.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import ci.smile.formationEcole.dao.entity.ClasseMatiere;

public interface IClasseMatiereRepository extends JpaRepository<ClasseMatiere, Integer>{

	@Query("select c from ClasseMatiere c where c.id= :id")
	ClasseMatiere findOne(@Param("id")Integer id);
}
