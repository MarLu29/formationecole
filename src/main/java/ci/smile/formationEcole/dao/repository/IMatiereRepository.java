package ci.smile.formationEcole.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import ci.smile.formationEcole.dao.entity.Matiere;

public interface IMatiereRepository extends JpaRepository<Matiere, Integer>{

	@Query("select m from Matiere m where m.id = :id")
	Matiere findOne(@Param("id")Integer id);
}
