package ci.smile.formationEcole.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import ci.smile.formationEcole.dao.entity.EleveMatiere;

public interface IEleveMatiereRepository extends JpaRepository<EleveMatiere, Integer>{

	@Query("select em from EleveMatiere em where em.id= :id")
	EleveMatiere findOne(@Param("id")Integer id);
}
