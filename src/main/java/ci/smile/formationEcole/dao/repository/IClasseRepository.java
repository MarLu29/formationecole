package ci.smile.formationEcole.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import ci.smile.formationEcole.dao.entity.Classe;
import ci.smile.formationEcole.dao.entity.Eleve;

public interface IClasseRepository extends JpaRepository<Classe, Integer>{

	@Query("select c from Classe c where c.id= :id")
	Classe findOne(@Param("id")Integer id);
}
