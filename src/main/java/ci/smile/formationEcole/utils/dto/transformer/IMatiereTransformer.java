package ci.smile.formationEcole.utils.dto.transformer;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import ci.smile.formationEcole.dao.entity.Matiere;
import ci.smile.formationEcole.utils.dto.MatiereDto;

@Mapper
public interface IMatiereTransformer {
	
	IMatiereTransformer INSTANCE = Mappers.getMapper( IMatiereTransformer.class );
	
	//tranformation de DTO en ENTITY
	@Mappings({ 
		@Mapping(source = "dto.id", target = "id"),
		@Mapping(source = "dto.libelle", target = "libelle"),
		})
	Matiere toEntity(MatiereDto dto);
	
	
	@Mappings({ 
		@Mapping(source = "entity.id", target = "id"),
		@Mapping(source = "entity.libelle", target = "libelle"),
		})
	MatiereDto toDto(Matiere entity);
	List<MatiereDto> toDtos (List<Matiere> entities);
	
	
	

}
