package ci.smile.formationEcole.utils.dto.transformer;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import ci.smile.formationEcole.dao.entity.Classe;
import ci.smile.formationEcole.dao.entity.Eleve;
import ci.smile.formationEcole.utils.dto.EleveDto;

@Mapper
public interface IEleveTransformer {
	
	IEleveTransformer INSTANCE = Mappers.getMapper( IEleveTransformer.class );
	
	//tranformation de DTO en ENTITY
	@Mappings({ 
		@Mapping(source = "dto.id", target = "id"),
		@Mapping(source = "dto.matricule", target = "matricule"),
		@Mapping(source = "dto.nom", target = "nom"),
		@Mapping(source = "dto.prenom",target = "prenom"),
		@Mapping(source = "dto.sexe",target = "sexe"),
		@Mapping(source = "dto.dateNaissance", dateFormat = "dd/MM/yyyy", target = "dateNaissance"),
		@Mapping(source = "dto.classeId", target = "classeId"), 
		})
	Eleve toEntity(EleveDto dto, Classe classe);
	
	
	@Mappings({ 
		@Mapping(source = "entity.id", target = "id"),
		@Mapping(source = "entity.matricule", target = "matricule"),
		@Mapping(source = "entity.nom", target = "nom"),
		@Mapping(source = "entity.prenom",target = "prenom"),
		@Mapping(source = "entity.sexe",target = "sexe"),
		@Mapping(source = "entity.dateNaissance", dateFormat = "dd/MM/yyyy", target = "dateNaissance"),
		@Mapping(source = "entity.classeId", target = "classeId"), 
		})
	EleveDto toDto(Eleve entity);
	List<EleveDto> toDtos (List<Eleve> entities);
	
	
	

}
