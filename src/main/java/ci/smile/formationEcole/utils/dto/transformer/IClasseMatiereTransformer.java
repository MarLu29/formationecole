package ci.smile.formationEcole.utils.dto.transformer;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import ci.smile.formationEcole.dao.entity.Classe;
import ci.smile.formationEcole.dao.entity.ClasseMatiere;
import ci.smile.formationEcole.dao.entity.Matiere;
import ci.smile.formationEcole.utils.dto.ClasseMatiereDto;
import ci.smile.formationEcole.utils.dto.MatiereDto;

@Mapper
public interface IClasseMatiereTransformer {
	
	IClasseMatiereTransformer INSTANCE = Mappers.getMapper( IClasseMatiereTransformer.class );
	
	//tranformation de DTO en ENTITY
	@Mappings({ 
		@Mapping(source = "dto.id", target = "id"),
		@Mapping(source = "dto.classeId", target = "classeId"),
		@Mapping(source = "dto.coefficient", target = "coefficient"),
		@Mapping(source = "dto.matiereId", target = "matiereId"),
		})
	ClasseMatiere toEntity(ClasseMatiereDto dto);
	
	
	@Mappings({ 
		@Mapping(source = "entity.id", target = "id"),
		@Mapping(source = "entity.classeId", target = "classeId"),
		@Mapping(source = "entity.coefficient", target = "coefficient"),
		@Mapping(source = "entity.matiereId", target = "matiereId"),
		})
	ClasseMatiereDto toDto(ClasseMatiere entity);
	List<ClasseMatiereDto> toDtos (List<ClasseMatiere> entities);
	
	
	

}
