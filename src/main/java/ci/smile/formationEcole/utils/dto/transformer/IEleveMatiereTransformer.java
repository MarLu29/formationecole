package ci.smile.formationEcole.utils.dto.transformer;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import ci.smile.formationEcole.dao.entity.EleveMatiere;
import ci.smile.formationEcole.utils.dto.EleveMatiereDto;

@Mapper
public interface IEleveMatiereTransformer {
	
	IEleveMatiereTransformer INSTANCE = Mappers.getMapper( IEleveMatiereTransformer.class );
	
	//tranformation de DTO en ENTITY
	@Mappings({ 
		@Mapping(source = "dto.id", target = "id"),
		@Mapping(source = "dto.note", target = "note"),
		@Mapping(source = "dto.eleveId", target = "eleveId"),
		@Mapping(source = "dto.matiereId", target = "matiereId"),
		})
	EleveMatiere toEntity(EleveMatiereDto dto);
	
	
	@Mappings({ 
		@Mapping(source = "entity.id", target = "id"),
		@Mapping(source = "entity.classeId", target = "classeId"),
		@Mapping(source = "entity.coefficient", target = "coefficient"),
		@Mapping(source = "entity.matiereId", target = "matiereId"),
		})
	EleveMatiereDto toDto(EleveMatiere entity);
	List<EleveMatiereDto> toDtos (List<EleveMatiere> entities);
	
	
	

}
