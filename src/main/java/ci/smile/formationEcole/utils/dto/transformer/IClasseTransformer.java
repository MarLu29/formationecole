package ci.smile.formationEcole.utils.dto.transformer;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import ci.smile.formationEcole.dao.entity.Classe;
import ci.smile.formationEcole.utils.dto.ClasseDto;

@Mapper
public interface IClasseTransformer {
	
	IClasseTransformer INSTANCE = Mappers.getMapper( IClasseTransformer.class );
	
	//tranformation de DTO en ENTITY
	@Mappings({ 
		@Mapping(source = "dto.id", target = "id"),
		@Mapping(source = "dto.libelle", target = "libelle"),
		@Mapping(source = "dto.nombrePlace", target = "nombrePlace"),
		})
	Classe toEntity(ClasseDto dto);
	
	
	@Mappings({ 
		@Mapping(source = "entity.id", target = "id"),
		@Mapping(source = "entity.libelle", target = "libelle"),
		@Mapping(source = "entity.nombrePlace", target = "nombrePlace"),
		})
	ClasseDto toDto(Classe entity);
	List<ClasseDto> toDtos (List<Classe> entities);
	
	
	

}
