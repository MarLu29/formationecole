
/*
 * Created on 2020-02-05 ( Time 19:10:56 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2018 Geo. All Rights Reserved.
 */

package ci.smile.formationEcole.utils.contract;

import java.util.Map;

import ci.smile.formationEcole.utils.Status;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

//import ci.smile.formationEcole.utils.Status;

/**
 * Response Base
 * 
 * @author Geo
 *
 */
@Data
@ToString
@NoArgsConstructor
public class ResponseBase {

	protected Status	status = new Status();
	protected boolean	hasError;
	protected String	sessionUser;
	protected Long		count;
	protected Map<String, Object>	result;
}
