package ci.smile.formationEcole.utils.contract;

import java.util.Locale;

public interface IBasicBusiness<T,K> {
	
	public K create (T request, Locale locale);
	public K update (T request, Locale locale);
	public K delete (T request, Locale locale);
	public K getByCriteria (T request, Locale locale);

}
