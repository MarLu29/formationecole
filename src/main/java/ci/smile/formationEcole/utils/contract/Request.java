package ci.smile.formationEcole.utils.contract;

import java.util.List;

import lombok.Data;

@Data
public class Request<T> extends RequestBase {
 private T data;
 private List<T> datas;
}
