package ci.smile.formationEcole.utils.contract;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;
@Data
@JsonInclude(Include.NON_NULL)
public class Response<T> extends ResponseBase {
	private T item;
	private List<T> items;

}
