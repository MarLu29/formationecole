/*
 * Created on 2020-02-05 ( Time 19:10:56 )
 * Generator tool : Telosys Tools Generator ( version 3.0.0 )
 * Copyright 2018 Geo. All Rights Reserved.
 */

package ci.smile.formationEcole.utils.contract;

import lombok.*;

/**
 * Request Base
 * 
 * @author Geo
 *
 */
@Data
@ToString
@NoArgsConstructor
public class RequestBase {
	protected String		sessionUser;
	protected Integer		size;
	protected Integer		index;
	protected String		lang;
	protected String		businessLineCode;
	protected String		caseEngine;
	protected Boolean		isAnd;
	protected Integer		user;
	protected Boolean 		isSimpleLoading;
	protected Boolean 		hierarchyFormat;
	protected Boolean   	isDashboard;
	protected Boolean 		isHour;
	protected Boolean 		isDay;
	protected Boolean 		isMonth;
	protected Boolean 		isYear;
	protected Boolean       isFormating;
	protected Boolean       withInfo;
	private   String 		groupeMobile;
	protected Boolean 		isForGraph;
	protected Boolean 		isAll;
	protected Boolean 		full;

}