package ci.smile.formationEcole;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FormationEcoleApplication {

	public static void main(String[] args) {
		SpringApplication.run(FormationEcoleApplication.class, args);
		System.out.println();
	}

}
